<?php
    include_once "DBConnect.php";
    function  buiding_menu($parent_id, $menuData){
        if(isset($menuData['parent_id'][$parent_id])){

            echo "<ul class='nav'>";
            if($parent_id == '0') { 
                echo '<li><a href="index.php">Home</a></li>';
            }
            foreach($menuData['parent_id'][$parent_id] as $value){
                echo "<li>";
                if($parent_id != '0') { 
                    echo "<a class='dropdown-item' href='index.php?page=productByLoaiSP&id=".$menuData['items'][$value]['id']."'>".$menuData['items'][$value]['name']."</a>";
                } else {
                    echo "<a class='dropdown-item' href='#' onclick='return false;'>".$menuData['items'][$value]['name']."</a>";
                }
                
                echo buiding_menu($value, $menuData);
                echo "</li>";
            }
            if($parent_id == '0') { 
                echo '<li><a href="index.php?page=feedback">Feedback</a></li>';
                echo '<li><a href="index.php?page=cart">Cart</a></li>';
            }
            echo "</ul>";
        }
       
    }

    $result=$db->select($connection,"SELECT * FROM catalog");
    foreach($result as $value){
        $menuData['items'][$value['id']]=$value;//Lưu dữ liệu các biến có id khác nh
        $menuData['parent_id'][$value['parent_id']][]=$value['id'];
    }


error_reporting( E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING );

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="style.css"/>
        <title></title>
    </head>
    <body>
        <!-- <?php
            echo buiding_menu(0,$menuData);
        ?> -->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">TASHA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto" style="margin: 0 auto;">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    
                    <?php foreach($menuData['parent_id'][0] as $value){ ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $menuData['items'][$value]['name'];?>
                            </a>
                            <!-- echo "<a class='dropdown-item' href='index.php?page=productByLoaiSP&id=".$menuData['items'][$value]['id']."'>".$menuData['items'][$value]['name']."</a>"; -->
                            <!-- <a class="nav-link dropdown-toggle" href="index.php?page=productByLoaiSP&id=<?php echo $menuData['items'][$value]['id']; ?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                            </a> -->
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                                <?php foreach($menuData['parent_id'][$value] as $value){ ?>
                                <a class="dropdown-item" style='color: white;' href="index.php?page=productByLoaiSP&id=<?php echo $menuData['items'][$value]['id']; ?>">
                                    <?php echo $menuData['items'][$value]['name'];?>
                                </a>
                                <?php } ?>
                            </div>
                        </li>

                    <?php } ?>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=feedback">Feedback</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=cart">Cart</a>
                    </li>
                </ul>
            </div>
        </nav>

    </body>
</html>
