<?php
    include_once './DBConnect.php';
    include_once './SanPhamController.php';
	
    $default = array(
        'data' => array(),
        'total' => 0
    );
    
    if(isset($_POST['change'])) {
        change($_POST['id'], $_POST['count']);
        // $cookie = get_cookie();
        // echo '<pre>';
        // var_dump($_POST['id']);
        // var_dump($cookie);
        // exit();
        // echo '</pre>';
        header('Location: index.php?page=cart');
        exit();
    }

    if(isset($_POST['remove'])) {
        remove($_POST['id']);
        $cookie = get_cookie();
        // echo '<pre>';
        // var_dump($_POST['id']);
        // var_dump($cookie);
        // exit();
        // echo '</pre>';
        header('Location: index.php?page=cart');
        exit();
    }

    if(isset($_POST['id'])) {
        $item = getDetailById($_POST['id']);
//		echo $_POST['id'];
//		echo '<pre>';
//		 var_dump($item);
//		 exit();
//		 echo '</pre>';
		
		
        add($item, 1);

        $cookie = get_cookie();
         
        header('Location: index.php?page=cart');
        exit();
    }

    function init_cookie() {
        $default = array(
            'data' => array(),
            'total' => 0
        );
        $init = $default;
        ltm_setcookie($init);
    }

    function get_cookie() {
        $cookie = null;
        if(!isset($_COOKIE['CART'])) {
            init_cookie();
        }
        $cookie = unserialize($_COOKIE['CART']);
        // echo '<pre>';
        // var_dump($cookie);
        // exit();
        // echo '</pre>';
        return $cookie;
    }
    function ltm_setcookie($cookie) {
        setcookie('CART', serialize($cookie), time() + 360000);
        
    }

    function remove_cookie() {
        init_cookie();
    }
    function add($item, $count) {
        //TODO implement
        $cookie = get_cookie();
        
        if(isset($cookie['data'][$item['id']])) {
            $product = $cookie['data'][$item['id']];
            $cookie['data'][$item['id']]['count'] += 1;
            // echo '<pre>';
            // var_dump($cookie['data'][$item['id']]['count']);
            // var_dump($cookie);
            // exit();
            // echo '</pre>';
        } else {
            $cookie['data'][$item['id']] = array(
                'item' => $item,
                'count' => 1
            );
        }
        ltm_setcookie($cookie);
    }
    
    function change($id, $count) {
        //TODO implement
        $cookie = get_cookie();
        
        if(isset($cookie['data'][$id])) {
            $product = $cookie['data'][$id];
            $product['count'] = $count;
        } else {
            $cookie['data'][$id] = array(
                'item' => $item,
                'count' => 1
            );
        }
        ltm_setcookie($cookie);
    }
//	session 

//	cookie['data']
//		
//		'data'
//			(item, count)
//			(item, count)
//		'total'
    function remove($id) {
        $cookie = get_cookie();
        unset($cookie['data'][$id]);
        ltm_setcookie($cookie);
    }

?>
