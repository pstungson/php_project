-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 22, 2018 lúc 06:34 PM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `kitchen`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `catalog`
--

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `catalog`
--

INSERT INTO `catalog` (`id`, `name`, `parent_id`) VALUES
(1, 'Cookwares', 0),
(2, 'Refrigerators', 0),
(3, 'Appliances', 0),
(4, 'Food Storage', 0),
(5, 'Cooking Pots', 1),
(6, 'Cooking Pans', 1),
(7, 'Frying Pans', 1),
(8, 'Cookware Sets', 1),
(9, 'Handis', 1),
(10, 'Kadais', 1),
(11, 'Grill Pans', 1),
(12, 'Tawas', 1),
(13, 'Egg Poachers', 1),
(14, 'Steamers', 1),
(15, 'Freezer', 2),
(16, 'Refrigerators', 2),
(17, 'Blenders', 3),
(18, 'Bread Makers', 3),
(19, 'Coffee Makers', 3),
(20, 'Ice-cream Makers', 3),
(21, 'Electric Kettles', 3),
(22, 'Food Choppers', 3),
(23, 'Toaster', 3),
(24, 'Flask', 4),
(25, 'Spice Jars', 4),
(26, 'Storage Bags', 4),
(27, 'Lunch Boxes', 4),
(28, 'Vacuum Bottles', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `checkout`
--

INSERT INTO `checkout` (`id`, `name`, `email`, `phone_number`, `address`) VALUES
(2, 'sdf', 'asd@ada', '123', '123'),
(3, 'Tạ Minh Luận', 'sdf@gmail.com', '234', 'Hà Nội'),
(4, 'Tạ Minh Luận', 'sdf@gmail.com', '123', 'Hà Nội'),
(5, 'Tạ Minh Luận', 'sdf@gmail.com', '123', 'Hà Nội'),
(6, 'Tạ Minh Luận', 'sdf@gmail.com', '123', 'Hà Nội'),
(7, 'Tạ Minh Luận', 'sdf@gmail.com', '123', 'Hà Nội');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `checkout_detail`
--

CREATE TABLE `checkout_detail` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `checkout_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `checkout_detail`
--

INSERT INTO `checkout_detail` (`id`, `product_id`, `quantity`, `checkout_id`) VALUES
(1, 20, 1, 6),
(2, 20, 1, 7),
(3, 18, 1, 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `email`, `content`) VALUES
(1, 'asdasd', 'sadasd', 'asdasd'),
(2, 'minh anh', '12345', 'xin chao'),
(3, 'hoang anh', 'hoanganh123@gmail.com', 'Website hay'),
(4, 'sadasd', 'a', 'asdadasd');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name_pr` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `link_img` varchar(100) NOT NULL,
  `price` int(11) DEFAULT '500000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name_pr`, `detail`, `parent_id`, `link_img`, `price`) VALUES
(5, 'Kopf Gigantos Large Cooking Pot with Lid, Stainless Steel, Diameter 26 cm, Height 19 cm, 10 Litre', 'Type: Large cooking pot (suitable for induction hobs) with lid - cooking for the whole family; dimen', 5, 'noinau1.jpg', 800000),
(6, 'Kopf Gigant Large Cooking Pot with Glass Lid, Stainless Steel, Diameter 28 cm, Height 21 cm, 12.5 Li', 'Properties: pot with practical spout, encapsulated sandwich base, 3.1 mm total base thickness, 0.6 m', 5, 'noinau2.jpg', 800000),
(7, 'Tefal Extra Frying Pan, 26 cm - Black', 'Thermospot technology\r\nPowerglide non-stick coating\r\nBakelite handle that provides a comfortable and secure grip\r\nTefal offers a lifetime guarantee on non-stick coating against any blistering and peeling, and the main body and lids of the pan are covered ', 7, 'chao1.jpg', 800000),
(8, 'Tefal Taste Twin Pack FryPans - Black', 'Tefal\'s enhanced Thermospot technology\r\nNon-stick coating\r\n20 cm fry pan and 28 cm fry pan\r\nPTFE non-stick exterior for easy cleaning\r\nDishwasher safe and suitable for all hobs except induction', 7, 'chao2.jpg', 800000),
(9, 'Circulon Premier Professional Hard Anodised 20/28 cm Frying Pan Twin Pack Set, 2-Piece - Black', 'Total non-stick interior that provides outstanding performance\r\nHeavy gauge hard anodised cookware heats quickly and spreads heat evenly\r\nStainless steel base for use on all hob types including induction\r\nAdvanced non-stick exterior.Comfortable silicone a', 7, 'chao3.jpg', 800000),
(10, 'Duxtop SSC-9PC 9 Piece Whole-Clad Tri-Ply Induction Cookware, Stainless Steel', 'Set includes 8-Inch, 10-Inch Fry Pan; 1.6-Quart and 3-Quart covered saucepan; 6-1/2-Quart saucepot with cover; 6.6\" pot holder\r\nWhole-Clad Tri-Ply Stainless Steel construction eliminates hot spots and ensures even heat distribution along the bottom and si', 8, 'bodonau1.jpg', 300000),
(11, 'Vremi 15 Piece Nonstick Cookware Set; 2 Saucepans and 2 Dutch Ovens with Glass Lids, 2 Fry Pans and 5 Nonstick Cooking Utensils; Oven Safe', 'ALL IN ONE NONSTICK COOKWARE - 15 pc kitchen cookware kit includes 4 pots with 4 glass lids and 2 sauce / frying pans. Non stick interior for an evenly heated simmer, sear or fry. Made from durable, non toxic food grade aluminum with enameled exterior VEN', 8, 'bodonau2.jpg', 300000),
(12, 'Funnytoday365 6 Pcs Kitchen Hob Magnetic Spice Jars With Stainless Steel Cookware Trestle Rack Kitchen Cooking Tools Cozinha Cocina Mutfak', '100% Brand New\r\nBest Cooking Tools\r\nHigh Quality Cooking Kitchen\r\nJar Kitchen', 25, 'lo1.jpg', 300000),
(13, 'DecoBros Spice Rack Stand holder with 18 bottles and 48 Labels, Chrome', 'Combo Pack inclues Spice Rack, 18 Spice Bottles and 48 Spice Labels\r\nIncludes 18 GLASS Spice Bottles,Each bottle 1-3/4 by 3-3/4 inches\r\nBuilt by Sturdy Steel with chrome finished\r\nDimension: 13\'\'L x 4 1/4\'\'W x 8 1/4\'\'H\r\nUSPTO Patent USD756177', 25, 'lo2.jpg', 300000),
(14, '6 Pcs 8 oz Hip Stainless Steel Flask & Funnel Set by QLL, Easy Pour Funnel is Included, Great Gift', '8 oz Stainless Steel Flask and Funnel Set of 6\r\nYou will receive 6 pieces Stainless Steel Flask and 6 pieces Funnel\r\nLaser Welded for a Leak Proof Seal, Lifetime Warranty\r\nGreat value, high quality flask-Very good quality flask - nice weight, not too heav', 24, 'binh1.jpg', 300000),
(15, 'American Flag Flask - Soft Touch Cover and Durable Construction | 18/8 304 Food Grade Stainless Steel | Leak Proof Slim Profile Classic American Flag Design | Funnel Included | Black | For Alcohol', 'PREMIUM QUALITY- Stainless Steel. Rugged, sleek leather wrapped with plenty of grip. 8oz\r\nAMERICAN FLAG DESIGN - with Leather Outer Cover that looks great at any event.\r\nLIGHTWEIGHT - will fit in your pocket discreetly with a slim contour shape. Great gif', 24, 'binh2.jpg', 100000),
(16, 'Eggssentials Poached Egg Maker - Nonstick 6 Egg Poaching Cups - Stainless Steel Egg Poacher Pan FDA Certified Food Grade Safe PFOA Free With Bonus Spatula', 'A HEALTHIER BREAKFAST ALTERNATIVE- PFOA FREE ANTI-STICK COATING- The only Egg Poaching Pan that has a PFOA free anti stick coating- From Eggs Benedict to Eggs Sardou Hard boiled eggs without the shell, Eggbits', 13, 'trung2.jpg', 100000),
(17, 'Egg Cooker, HoLife Stainless Steel Egg Poacher Boiler Steamer with Auto Shut off, 7 Egg Capacity for Soft, Medium, Hard Boiled Eggs, Omelettes', '7 EGG CAPACITY: 7 eggs capacity cooker boils eggs perfectly in minutes. Now you can enjoy your eggs the way you like in hard, medium, and soft boiled style.', 13, 'trung2.jpg', 100000),
(18, 'Dukers D28F 17.7 cu. ft. Single Door Commercial Freezer in Stainless Steel', 'Dukers Appliance USA', 15, 'tudong1.jpg', 100000),
(19, 'Kenmore 04674015 French Door Bottom Freezer Refrigerator with Active Finish, Stainless Steel, Elite 30.6 cu. ft, Stainless Steel with Active Finish', 'Item includes room of choice delivery, unboxing, item hook-up and free optional haul-away at checkout', 15, 'tudong1.jpg', 100000),
(20, 'Blendtec FourSide Jar', 'Ergonomic design and light weight makes easier pouring;Made from impact-resistant\r\nEasy-to-read jar markings up to 32 ounces or 4 cups (with a total volume of nearly 2 quarts', 17, 'sinhto1.jpg', 1000000),
(21, 'Blendtec Twister Jar', 'Designed for ultra-thick recipes, such as nut butters, hummus and more\r\nTwister Lid features tines that rotate and push ingredients off the side and into the blending vortex\r\nDurable BPA-free, Triton Polyester material will stand up to heavy use\r\nComes wi', 17, 'sinhto2.jpg', 1000000),
(22, 'Hamilton Beach (29882) Bread Maker, 2 Pound Capacity Bread Maker machine, Gluten Free Setting, Programmable, Black', '12 SETTINGS INCLUDING GLUTEN-FREE. Developed with nutrition in mind, settings include: French, Quick Bread (no yeast), Sweet, 1.5-lb Express, 2-lb Express, Dough, Jam, Cake, Whole Grain and Bake. Audible reminder for adding fruits and nuts.', 18, 'banhmi1.jpg', 1000000),
(23, 'SKG Automatic Bread Machine 2LB - Beginner Friendly Programmable Bread Maker', 'THE REAL PLEASURE OF HOMEMADE BREAD IS CLICKS AWAY - For newbies and pros alike, simply follow the Easy-to-Follow recipes to add ingredients, click a few buttons, and then let the bread maker take care of the rest (preheat, kneading, rising, baking, etc.)', 18, 'banhmi2.jpg', 1000000),
(24, 'Hamilton Beach (49980A) Single Serve Coffee Maker and Coffee Pot Maker, Programmable, Stainless Steel', 'Note: Travel mug not-included. Dimensions (inches): 13.9 H x 12.23 W x 11.08 D\r\nTwo ways to brew your coffee, single serve or full pot.\r\n12 cup glass carafe included or brew into a travel or regular sized mug using single serve function.', 19, 'ca1.jpg', 500000),
(25, 'Hamilton Beach (49976) Coffee Maker, Single Serve & Full Coffee Pot, For Use With K Cups or Ground Coffee, Programmable, FlexBrew', 'Two ways to brew. Brew a full pot using your favorite grounds on the carafe side, or make a cup for yourself using a Keurig* K-Cup pack or grounds on the single-serve side with this 2-In-1 Coffee maker. Dimensions (inches) : 13.9 H x 10.24 W x 10.63 D', 19, 'ca2.jpg', 500000),
(26, 'Ice Cream Maker, Sweet Alice 1.5 Quart Ice Cream Machine with Timer Auto Shut-off, BPA Free for Kids DIY Soft-Serve Ice Cream, Frozen Yogurt, Sorbet Maker for Home', 'Sweet Alice ice cream maker design with the high quality frozen liquid, so that the ice cream could be freezes better and faster. Put the ice cream inner bowl at -18°C and frozen for 8-12 hours before using,', 20, 'kem1.jpg', 500000),
(27, 'Ice Cream Maker, Sweet Alice 1.5 Quart Ice Cream Machine with Timer Auto Shut-off, BPA Free for Kids DIY Soft-Serve Ice Cream, Frozen Yogurt, Sorbet Maker for Home', ' Use Sweet Alice ice cream maker for homemade ice cream, there is NO additive NO pigment and NO preservatives, much healthier for you and your family ! You can control adding the amount of sugar and chocolate during the production process,', 20, 'kem2.jpg', 500000),
(28, 'Electric Kettle 1.7L Stainless Steel Tea Kettle with Auto Shut-Off, 1000W Fast Boiling Water Kettle, Hot Water Kettle Electric with Boil-Dry Protection By Homeleader', '1.7 LITER CAPACITY: At a maximum capacity of 1.7 L(8 cups), water can be fully boiled in 5-6 minutes. Save time than conventional stovetops. The humanized design for household use. Portable for office, home party, and gathering.', 21, 'am1.jpg', 500000),
(29, 'Homeleader Electric Kettle, Fast Boiling Glass Tea Kettle (BPA-Free), Cordless Electric Glass Kettle with Blue LED Lighting, Auto Shut-Off and Boil-Dry Protection, 1.7 Liter Capacity', 'EXCELLENT QUALITY: The materials are 100% BPA-Free, made with the superior quality borosilicate glass. It is not only hard and firm, but also does not have any odor or impurities to affect the quality of water when boiled.\r\nQUICK AND EASY OPERATION: Equip', 21, 'am2.jpg', 500000),
(30, 'ROYAL KASITE 10.5-Inch Preseasoned Cast Iron Square Grill Pan, Black', 'Made from heavy duty pre-seasoned cast iron\r\nCast Iron Grill Pan Measurement:10.5\" by 1.96\" by 16.5\"\r\nGreat for burgers,pancakes and steaks\r\nUsable in all kitchens: gas, electric, induction, oven, campfire, grill etc.\r\nHand wash with mild soap and warm wa', 11, 'chaonuong1.jpg', 500000),
(31, 'Cooksmark Copper Pan 10-Inch Nonstick Deep Square Grill Pan, Deep Griddle Pan with Stainless Steel Handle, Dishwasher Safe Oven Safe', 'This 10 x 10 inches nonstick copper ceramic deep square grill pan ensures you 100% safe and healthy cooking as Lead/Cadmium free. Enjoy perfect chargrilled foods from now on!', 11, 'chaonuong2.jpg', 500000),
(32, 'Brieftons QuickPull Food Chopper: Large 4-Cup Powerful Manual Hand Held Chopper / Mincer / Mixer / Blender to Chop Fruits, Vegetables, Nuts, Herbs, Onions for Salsa, Salad, Pesto, Coleslaw, Puree', 'DESIGNED FOR A PERFECT CHOPPING / BLENDING JOB: This hand food chopper takes the hard work out of chopping & blending vegetables, fruits, nuts and herbs. Features sharp stainless steel blades for fast and even chopping/mincing, comfort soft grip lid, anti', 22, 'thucpham1.jpg', 500000),
(33, 'Vegetable Chopper Slicer Dicer Cutter Peeler Cheese Grater. 12-in-1 (22 Pieces). Best Manual Mandoline, Food Chopper, Lemon Squeezer, Egg White Separator, Egg Slicer. Free Cut-Resistant Gloves Brushes', ' NEW 2018 DESIGN. THE MOST COMPLETE KITCHEN SET (22 Pieces) Don’t waste your hard-earned money for a cheap chopper or slicer! Cut down your meal prep time, free-up your kitchen space and enjoy a variety of healthy meals with this 12-in-1 Food Chopper Slic', 22, 'thucpham2.jpg', 500000),
(34, 'Oster 2-Slice Toaster, Black (TSSTTRJBG1-NP)', '2-slice toaster features extra-wide slots to accommodate a variety of items\r\nAuto-adjusting bread guides for even, consistent toasting\r\n7 toast shades from light to dark and 3 settings: bagel, frozen, and pastry\r\nRemovable crumb tray makes cleanup easy', 23, 'banhm1.jpg', 500000),
(35, 'Homeleader 2-Slice Toaster, Stainless Steel Toaster Oven with Removable Crumb Tray, Defrost/Reheat/Cancel Function, 7 Setting Shade Selectors, Extra-Wide Slots, 700W, Silve', 'The Stainless Steel Toasters has quick buttons for Reheat,Defrost and Cancel. 7 adjustable shade settings from 1(light) to 7(dark) let you brown any types of bread to your desired shade', 23, 'banhm.jpg', 500000),
(36, 'Clorox Handi Wipes Heavy Duty Reusable Cloths, 3 Count', 'Four 3-count packs (12 wipes total)\r\nSuperabsorbent, multipurpose wipes\r\nCan be machine washed and reused\r\nRinse and reuse up to 20 times\r\nAn alternative to paper towels, rags and sponges', 7, 'ha2.jpg', 500000),
(37, 'Handi Wipes Reusable Cloths, Extra Large 6 cloths', 'Handi Wipes Reusable Cloths, Extra Large 6 cloths', 7, 'ha2.jpg', 500000),
(38, 'Cuisinart 755-26GD Chef\'s Classic Stainless 5-1/2-Quart Multi-Purpose Pot with Glass Cover', 'Stainless steel, classic looks and professional performance\r\nCooking surface does not discolor, react with food, or alter flavors\r\nSolid stainless steel riveted handles stay cool on the stove top\r\nAluminum encapsulated base heats quickly and spreads heat ', 10, 'ka2.jpg', 500000),
(39, 'Lodge P14W3 Seasoned Cast Iron Wok, 14 inch', '14 inch diameter Cast Iron Wok\r\nSturdy flat base keeps wok stationary on the stovetop.Item Weight: 14 pounds\r\nUse on Electric, Gas or Induction stove top. Can be used in the oven\r\nSeasoned with oil for a natural, easy-release finish that improves with use', 10, 'ka1.jpg', 500000);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `checkout_detail`
--
ALTER TABLE `checkout_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `checkout_id` (`checkout_id`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `checkout_detail`
--
ALTER TABLE `checkout_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `checkout_detail`
--
ALTER TABLE `checkout_detail`
  ADD CONSTRAINT `checkout_detail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `checkout_detail_ibfk_2` FOREIGN KEY (`checkout_id`) REFERENCES `checkout` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
