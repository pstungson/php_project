<footer class="page-footer font-small mdb-color lighten-3 pt-4">
    <div class="container text-center text-md-left">
      <div class="row">
        <div class="col-md-4 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1">
          <h5 class="font-weight-bold text-uppercase mb-4">The Tasha concept</h5>
          <p> Tasha retailing started as
an idea in a kitchen of
southern Ha Noi in 2013. </p>
          <p>What today
is the Tasha Concept has
evolved during many years
and is still improving.</p>
        </div>
        <hr class="clearfix w-100 d-md-none">
        <div class="col-md-2 col-lg-2 mx-auto my-md-4 my-0 mt-4 mb-1">
          <h5 class="font-weight-bold text-uppercase mb-4">Support</h5>

          <ul class="list-unstyled">
            <li>
              <p>
                <a href="index.php?page=manuals">Manuals</a>
              </p>
            </li>
            <li>
              <p>
                <a href="index.php?page=tips"> Expert Tips</a>
              </p>
            </li>
            <li>
              <p>
                <a href="index.php?page=feedback">Feedbacks</a>
              </p>
            </li>
			  <li>
              <p>
             <a href="index.php?page=contacts">Contact</a>
              </p>
            </li>
          </ul>

        </div>
        <hr class="clearfix w-100 d-md-none">
        <div class="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">
          <h5 class="font-weight-bold text-uppercase mb-4">Address</h5>

          <ul class="list-unstyled">
            <li>
              <p><i class="fa fa-home mr-3"></i> 350 Doi Can Str, Ba Dinh Dis, Ha Noi</p>
              
            </li>
          </ul>
          <div id="map" style="height: 270px;">Loading map...</div>
        </div>
                <hr class="clearfix w-100 d-md-none">
        <div class="col-md-2 col-lg-2 text-center mx-auto my-4">
          <h5 class="font-weight-bold text-uppercase mb-4">Location</h5>
          <a type="button" class="btn-floating btn-fb">
            <i class="fa fa-facebook"></i>
          </a>
                 <a type="button" class="btn-floating btn-tw">
            <i class="fa fa-twitter"></i>
          </a>
                  <a type="button" class="btn-floating btn-gplus">
            <i class="fa fa-google-plus"></i>
          </a>
                    <a type="button" class="btn-floating btn-instagram">
            <i class="fa fa-instagram"></i>
          </a>

        </div>
              </div>
          </div>
        <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="index.php"> tasha.com</a>
    </div>
    </footer>
    <script>
      function myMap() {
        var mapOptions = {
            center: new google.maps.LatLng(21.0363233,105.8172949),
            zoom: 15,
            mapTypeId: 'roadmap'
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyDYirJBIbSTqtL2dmrCeLhnAs3jocH73xM"></script>
    
</body>
</html>
  