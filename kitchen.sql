/*
 Navicat Premium Data Transfer

 Source Server         : DBlocal
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : localhost:3306
 Source Schema         : kitchen

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 10/08/2018 15:50:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for catalog
-- ----------------------------
DROP TABLE IF EXISTS `catalog`;
CREATE TABLE `catalog`  (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of catalog
-- ----------------------------
INSERT INTO `catalog` VALUES (1, 'Cookwares', 0);
INSERT INTO `catalog` VALUES (2, 'Refrigerators', 0);
INSERT INTO `catalog` VALUES (3, 'Appliances', 0);
INSERT INTO `catalog` VALUES (4, 'Food Storage', 0);
INSERT INTO `catalog` VALUES (5, 'Cooking Pots', 1);
INSERT INTO `catalog` VALUES (6, 'Cooking Pans', 1);
INSERT INTO `catalog` VALUES (7, 'Frying Pans', 1);
INSERT INTO `catalog` VALUES (8, 'Cookware Sets', 1);
INSERT INTO `catalog` VALUES (9, 'Handis', 1);
INSERT INTO `catalog` VALUES (10, 'Kadais', 1);
INSERT INTO `catalog` VALUES (11, 'Grill Pans', 1);
INSERT INTO `catalog` VALUES (12, 'Tawas', 1);
INSERT INTO `catalog` VALUES (13, 'Egg Poachers', 1);
INSERT INTO `catalog` VALUES (14, 'Steamers', 1);
INSERT INTO `catalog` VALUES (15, 'Freezer', 2);
INSERT INTO `catalog` VALUES (16, 'Refrigerators', 2);
INSERT INTO `catalog` VALUES (17, 'Blenders', 3);
INSERT INTO `catalog` VALUES (18, 'Bread Makers', 3);
INSERT INTO `catalog` VALUES (19, 'Coffee Makers', 3);
INSERT INTO `catalog` VALUES (20, 'Ice-cream Makers', 3);
INSERT INTO `catalog` VALUES (21, 'Electric Kettles', 3);
INSERT INTO `catalog` VALUES (22, 'Food Choppers', 3);
INSERT INTO `catalog` VALUES (23, 'Toaster', 3);
INSERT INTO `catalog` VALUES (24, 'Flask', 4);
INSERT INTO `catalog` VALUES (25, 'Spice Jars', 4);
INSERT INTO `catalog` VALUES (26, 'Storage Bags', 4);
INSERT INTO `catalog` VALUES (27, 'Lunch Boxes', 4);
INSERT INTO `catalog` VALUES (28, 'Vacuum Bottles', 4);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL,
  `name_pr` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `link_img` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, 'Banh My', 'Day la banh my nghe an', 1, 'http://cachlammonngon.net/wp-content/uploads/2015/11/banh-mi-kep-thit-bo.jpg');

SET FOREIGN_KEY_CHECKS = 1;
