<?php
// date: 2018/08/12
// Category controller
include_once './DBConnect.php';

function getAllCategory() {
    $link = open();
    $categories = array();
    $id = addslashes($_GET['id']);
    if ($link) {
        $sql = "select * from catalog";
        $mysqli_query = mysqli_query($link, $sql);
        $i = 0;
        while ($row = mysqli_fetch_array($mysqli_query, MYSQLI_ASSOC)) {
            $categories[$i] = $row;
            $i++;
        }
        close($link);
    } else {
        echo "Khong ket noi dc den db";
        die();
    }
    return $categories;
}

function getChildren($id) {
    $link = open();
    $categories = array();
    if ($link) {
        $sql = "select * from catalog where parent_id = ".$id;
        $mysqli_query = mysqli_query($link, $sql);
        $i = 0;
        while ($row = mysqli_fetch_array($mysqli_query, MYSQLI_ASSOC)) {
            $categories[$i] = $row;
            $i++;
        }
        close($link);
    } else {
        echo "Khong ket noi dc den db";
        die();
    }
    return $categories;
}

function getDetailCategory($id) {
    $link = open();
    $row = array();
    if ($link) {
        $sql = "select * from catalog where id = ".$id;
        // var_dump($sql);
        // die();
        $mysqli_query = mysqli_query($link, $sql);
        $row = mysqli_fetch_assoc($mysqli_query);

        close($link);
    } else {
        echo "Khong ket noi dc den db";
        die();
    }
    return $row;
}
