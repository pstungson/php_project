<?php
    include_once './SanPhamController.php';
    include_once './CategoryController.php';
?>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="width: 100%">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="img/1.jpg" alt="First slide" href="">
    </div>
    <div class="carousel-item">
        <img class="d-block w-100" src="img/2.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
        <img class="d-block w-100" src="img/3.jpg" alt="Third slide"  onClick="window.location='https://google.com.vn'"></div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

    <div class="container">
        <section class="blog bgwhite p-t-94 p-b-65">
            <div class="container">
                <div class="sec-title p-b-52">
                    <h3 class="m-text5 t-center">
                      Our Products
                    </h3>
                </div>
            <div class="row">
                <?php
                    $sanPhamByLoai = getAll();
                    foreach ($sanPhamByLoai as $value) {
                        $masp = $value['id'];
                        $tensp = $value['name_pr'];
                        $mota = $value['detail'];
                        $anh = $value['link_img'];
                ?>
                <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
                      <!-- Block3 -->
                    <div class="block3">
                        <a href="index.php?page=product&id=<?php echo $masp;?>" class="block3-img dis-block hov-img-zoom">
                            <img src="img/<?php echo $anh?>"  style="height:220px" alt="IMG-BLOG">
                        </a>
                        <div class="block3-txt p-t-14">
                            <h4 class="p-b-7">
                                <a href="index.php?page=product&id=<?php echo $masp;?>" class="m-text11">
                                    <?php echo $tensp;  ?>
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
                <?php
                  }
                ?>
              </div>
            </div>
        </section>
    </div>
