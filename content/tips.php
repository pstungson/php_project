<div class="container">
<h1 align="center"><span style="color: blue" >Helpful Kitchen Cookware Tips</span> </h1>
<br>
    <br>
    <h3><span style="color: blue">Nonstick Cookware</span></h3>
    <br>
    <br>
Dishwasher Safe 
    <br>
    Oven safe to 500ºF <strong>if handles are all stainless steel.</strong>
    <br>
    Oven safe to 350 ºF <strong>if handles have any silicone or phenolic parts</strong>
    <br>
    <i>Expert tip</i>: even if the nonstick interior is metal utensil safe to prolong the life of your cookware use wooden, silicone, or plastic utensils 
    <br>
    <br>
    <hr width="50%" align="center" / color="black">
    <br>
    <br>
    <h3><span style="color: blue">Porcelain Enamel</span></h3>
    <br>
    <br>
Dishwasher Safe
    <br>
Oven safe to 500ºF <strong>if handles are all stainless steel.</strong>
    <br>
Oven safe to 350 ºF <strong>if handles have any silicone or phenolic parts.</strong>
    <br>
    <i>Expert tip</i>: While Porcelain enamel cookware is dishwasher safe, avoid harsh detergents and do not crowd in the dishwasher as this may dull the enamel finish over time. Hand washing will also protect the color over time.
    <br>
    <br>
    <hr width="50%" align="center" / color="black">
    <br>
    <br>
    <h3><span style="color: blue">Stainless Steel</span></h3>
    <br>
    <br>
Dishwasher Safe
    <br>
Oven safe to 500ºF <strong>if handles are all stainless steel.</strong>
    <br>
Oven safe to 350 ºF <strong>if handles have any silicone or phenolic parts.</strong>
    <br>
    <i>Expert tip</i>: Baking soda or BarKeeper’s Friend helps restore the stainless steel beautiful shine. 
    <br>
    <br>
    <hr width="50%" align="center" / color="black">
    <br>
    <br>
<h3><span style="color: blue">Hard Anodized </span></h3>
    <br>
    <br>
Hand wash only 
    <br>
Oven safe to 500ºF <strong>if handles are all stainless steel.</strong>
    <br>
Oven safe to 350 ºF <strong>if handles have any silicone or phenolic parts.</strong>
    <br>
<i>Expert tip</i>: Hard anodized is one of the most durable cookware materials. Over time the exterior may discolor. However, this does not affect the performance.
    <br>
    <br>
    <hr width="50%" align="center" / color="black">
    <br>
    <br>
<h3><span style="color: blue">Using cooking and serving utensils</span></h3>
    <br>
    <br>
<strong>WearEver cookware nonstick interiors are metal utensil safe. However, avoid using sharp edged utensils on nonstick interiors.</strong>
    <br>
Do not cut in the pans. Do not gouge the cooking surface. Extremely sharp utensils, such as knives, may scratch the coating surface.
    <br>
Wooden, plastic, or silicone utensils are very gentle and are recommended to maintain the coating surface over time. 
    <br>
    <br>
    <hr width="50%" align="center" / color="black">
    <br>
    <br>
<h3><span style="color: blue">Caring for the glass lids</span></h3>
    <br>
    <br>
<strong>Warning: Failure to follow these warnings may cause the product to suddenly fracture into many small pieces that could result in property damage or serious personal injury from cuts or burns.</strong>
    <br>
    <br>
Avoid impact to the glass lid.
    <br>
Handle with care.
    <br>
Do not place glass lids or bowls directly on top of a heating element or under the broiler.
    <br>
Do not use harsh cleaning pads of cleansers that can scratch and weaken the glass.<br>
Do not use if chipped, cracked or noticeably scratched.<br>
Do not handle a hot lid with a wet cloth or place on a wet or cold surface.<br>
Do not use hard sharp knives or utensils that can scratch and weaken the glass. Avoid sudden temperature changes.<br>
Do not immerse hot glass in water. Clean burned on foods by soaking in hot water and dish detergent. You may also use a non-abrasive cleaning pad.
    <br>
    <br>
    <hr width="50%" align="center" / color="black">
    <br>
    <br>
<h3><span style="color: blue">Cookware Storage</span></h3>
    <br>
    <br>
Hang your cookware on a cookware rack or stack it in a cabinet with a lot of breathing room.
    <br>
    <br>
    <hr width="50%" align="center" / color="black">
    <br>
    <br>
    </div>
