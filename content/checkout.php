<?php
include_once './CheckoutController.php';
?>
<div class="container">
    <section class="bgwhite p-t-60 p-b-25">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9 p-b-80">
                    <div class="p-r-50 p-r-0-lg">
                        <form class="leave-comment" method="post" action="CheckoutController.php">
                            <h4 class="m-text25 p-b-14">
                                Checkout
                            </h4>
                            <label for="name">Name:</label>
                            <div class="bo12 of-hidden size1 m-b-20">
                                <input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="name" placeholder="Name *" required="required">
                            </div>

                            <label for="email">Email:</label>  
                            <div class="bo12 of-hidden size1 m-b-20">
                                <input class="sizefull s-text7 p-l-18 p-r-18" type="email" name="email" placeholder="Email *" required="required">
                            </div>

                            <label for="email">Phone number:</label>  
                            <div class="bo12 of-hidden size1 m-b-20">
                                <input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="phone-number" placeholder="Phone number *" required="required">
                            </div>

                            <label for="email">Address:</label>  
                            <div class="bo12 of-hidden size1 m-b-20">
                                <input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="address" placeholder="Address *" required="required">
                            </div>

                            <div class="w-size24">
                                <!-- Button -->
                                <button type="submit" name="checkout_success" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    Checkout
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
