<?php
    include_once './SanPhamController.php';
    include_once './CategoryController.php';
    $product = getDetail();
?>
<div class="container">
    
<section class="bgwhite p-t-60 p-b-25">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9 p-b-80">
                    <div class="p-r-50 p-r-0-lg">
                        <div class="p-b-40">
                            <div class="blog-detail-img wrap-pic-w">
                                <img src="img/<?php
echo $product['link_img'];
?>" alt="IMG-BLOG">
                            </div>
                            <div class="blog-detail-txt p-t-33">
                                <h4 class="p-b-11 m-text24">
                                    <?php
echo $product["name_pr"];
?>
                               </h4>

                                <div class="s-text8 flex-w flex-m p-b-21">
                                    <span>
                                        By Admin
                                        <span class="m-l-3 m-r-6"></span>
                                    </span>
                                </div>

                                <p class="p-b-25">
                                    <?php
echo $product['detail'];
?>
                               </p>
                            </div>
                            <div class="blog-detail-txt p-t-33 pull-right">
                                <form action="CartController.php" method="post">
                                    <input type="hidden" name="id" value="<?php
echo number_format($product['id']);
?>"/>
                                    <button class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4" 
                                    style="padding: 0 20px; cursor: pointer;" type="submit">
                                        Buy <?php
echo number_format($product['price']);
?> VND
                                    </button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-lg-3 p-b-80">
                    <div class="rightbar">
                        <!-- Search --> 

                        <!-- Categories -->
                        <h4 class="m-text23 p-t-56 p-b-34">
                            Categories
                        </h4>

                        <ul>
                        <?php
$category   = getDetailCategory($product['parent_id']);
$categories = getChildren($category['parent_id']);
foreach ($categories as $item) {
    $id   = $item['id'];
    $name = $item['name'];
?>
                           <li class="p-t-6 p-b-8 bo6">
                                <a href="index.php?page=productByLoaiSP&id=<?php
    echo $id;
?>" class="s-text13 p-t-5 p-b-5">
                                    <?php
    echo $name;
?>
                               </a>
                            </li>
                        <?php
}
?>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
