<?php
    include_once './CartController.php';
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <h2 style="text-align:center; padding:20px;">Cart</h2>
        </div>
    </div>
    <?php 
        $cart = get_cookie();
        if( count($cart['data']) == 0 ) {
    ?>
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <h2>Cart is empty.   <a href="index.php">Continue</a></h2>
            </div>
        </div>
    <?php
        } else {
    ?>
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Total</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0;?>
                        <?php
                            foreach ($cart['data'] as $item) {
                                $product = $item['item'];
                                $count = $item['count'];
                            ?>
                                <tr>
                                    <td class="col-sm-8 col-md-6">
                                    <div class="media">
                                        <a class="thumbnail pull-left" href="index.php?page=product&id=<?php echo $product['id']; ?>"> <img class="media-object" src="img/<?php echo $product['link_img'];?>" style="width: 72px; height: 72px; padding-right:10px;"> </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="index.php?page=product&id=<?php echo $product['id']; ?>"><?php echo $product['name_pr']; ?></a></h4>
                                        </div>
                                    </div></td>
                                    <td class="col-sm-1 col-md-1" style="text-align: center">
                                    <input type="email" class="form-control" id="exampleInputEmail1" value="<?php echo $count; ?>" onblur="change(<?php echo $product['id'];?>)">
                                    </td>
                                    <td class="col-sm-1 col-md-1 text-center"><strong><?php echo number_format($product['price']); ?> VND</strong></td>
                                    <td class="col-sm-1 col-md-1 text-center"><strong><?php echo number_format($count * $product['price']); $total = $total + $count * $product['price'];?> VND</strong></td>
                                    <td class="col-sm-1 col-md-1">
                                    <form action="CartController.php" method="post">
                                        <input type="hidden" name="id" value="<?php echo $product['id'];?>">
                                        <input type="hidden" name="remove" value="true">
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </button></td>
                                    </form>
                                    
                                </tr>
                            <?php
                            }
                        ?>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td><h3>Total</h3></td>
                            <td class="text-right"><h3><strong><?php echo number_format($total);?> VND</strong></h3></td>
                        </tr>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td>
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-shopping-cart"></span> <a href="index.php">Continue Shopping</a> 
                            </button></td>
                            <td>
                            <form action="CheckoutController.php" method="post">
                                <button type="submit" class="btn btn-success" name="checkout">
                                    Checkout <span class="glyphicon glyphicon-play"></span>
                                </button></td>
                            </form>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }
?>
</div>
