<div class="container">
    <span style="color: blue"><h1 align="center">Cookware Use and Care</h1></span>
    <br>
    <br>
    <span style="color: red"><strong>IMPORTANT SAFETY, USE AND CARE INFORMATION</strong></span>
<br>
Thank you for choosing our cookware. To assure a long and pleasant experience, please read this information before you use your cookware. These instructions are for your general safety, use and care to avoid personal injury and damage to your cookware. Please note that some sections may not apply to your specific line of cookware.
<br>
<i>Note: This Use and Care makes reference to porcelain enamel and silicone polyester coated cookware. In general, you can tell if your cookware is porcelain enamel or silicone polyester coated if it has a colored exterior</i>
    <br>
    <br>
    <strong><a>COOKING</a></strong>
    <br>
    <br>
    <span style="color: blue">General:</span>
    <br>
    <ul>
<li><strong>Safety</strong>: Keep small children away from the stove while you are cooking. Never allow a child to sit near the stove while cooking. Be careful around the stove as heat, steam and splatter can cause burns.</li>
<li><strong>Pet Warning</strong>: Fumes from everyday cooking can be harmful to your bird and other small pets, particularly smoke from burning foods. Overheating cooking sprays, oils, fats, margarine and butter will create dangerous fumes which a bird’s respiratory system cannot handle. Scorched plastic handles or utensils can also contaminate the air and endanger birds and other pets. Nonstick cookware with polytetrafluoroethylene (PTFE) coating can also emit fumes harmful to birds. Never allow pan to overheat and never cook with birds or small pets in or near the
    kitchen.</li>
<li><strong>Unattended cooking</strong>: WARNING: Never allow your cookware to boil dry and never leave an empty pan on a hot burner. An unattended, empty pan on a hot burner can get extremely hot, which can cause personal injury and/or property damage.</li>
    </ul>
<strong>1. Aluminum with stainless or copper base</strong>: In some cases, cookware with aluminum bases that are allowed to boil dry may melt and separate resulting in personal injury and property damage. If your cookware boils dry and you see molten aluminum, do not pick up your cookware. Turn burner off, leave cookware on the stove, and let cool before moving.
    <br>
<strong>2. Clad stainless steel</strong>: These pans may become discoloured or warped if allowed to boil dry.
    <br>
<strong>3. Porcelain enamel or silicone polyester</strong>: Overheating or allowing to boil dry can make cookware fuse to the surface of glass top ranges, requiring the replacement of the glass cook top. Never leave porcelain enamel cookware on the glass cook top. Remove to a heat-resistant surface such as a wooden cutting board.
    <br>
    <ul style="list-style-type: square"></ul>
<li><strong>Match pan size to burner size</strong>: Use burners that are same size as the cookware you are using. Adjust gas flame so that it does not extend up the sides of the cookware.</li>
<li><strong>Sliding Pans</strong>: Avoid sliding or dragging your cookware over the surface of your stovetop, especially glass top ranges, as scratches may result. We are not responsible for scratched stovetops.</li>
<li><strong>Microwaves</strong>: Never use your cookware in the microwave.</li>
<li><strong>Oven Use</strong>: Caution: Always use potholders when removing cookware from the oven. All cookware is oven safe to 180°C or 350°F. Cookware with stainless steel handles is oven safe to 260°C or 500°F. If cookware with a stainless steel handle is used with a glass lid, it is oven safe up to 200°C or 400°F. <i>NOTE: Some cookware may be safe at other temperatures depending upon the product specifics. See packaging for specifics. Oven use may cause rubberized handles to slightly fade in color but will not affect performance. This is not covered under warranty.</i></li>
<li><strong>Broiler</strong>: Never place nonstick cookware under the broiler. Cookware with metal handles and no nonstick is broiler-safe.</li>
<li><strong>Utensils</strong>: Do not use metal or sharp-edged utensils, which will scratch both stainless steel and nonstick surfaces. Scratches and peeling due to metal utensil use is not covered under warranty.</li>
<li>This cookware is not intended for commercial use or restaurant use.</li>
<li>Do not make a double boiler out of cookware that is not designed for that purpose.</li>
    <br>
    <span style="color: blue">Non-Stick Cookware:</span>
    <br>
<strong>Standard care</strong>: Use low to medium heat only. Excessive use of high heat will cause pan warping and permanent nonstick coating damage. Use of high heat and resulting nonstick damage is not covered under your warranty.
<br>
<strong>Nonstick Sprays</strong>: Do not use nonstick cooking sprays on nonstick cookware – an invisible buildup will impair the nonstick release system and food will stick in your cookware.
    <br>
    
<strong>Oil</strong>: Oil is not needed on nonstick cookware, but if you prefer oil for flavor, olive oil or peanut oil is recommended. Heavy vegetable oils may leave a residue that can affect nonstick performance. Use low heat when heating up oils. Oils can quickly overheat and cause a fire.
    <br>
    <br>
    <strong><a>CLEANING</a></strong>
    <br>
    <br>
<strong>Standard Care:</strong>
    <br>
<strong>1. All cookware</strong>: Before first use and after each use, wash the cookware thoroughly with mild dishwashing detergent and warm water. If food remains on the surface, boil a mixture of water and vinegar into the cookware to dislodge the food particles.
    <br>
<strong>Copper bottom cookware</strong>: A protective layer has been applied to the copper base to prevent tarnishing during packaging. BEFORE FIRST USE, remove this layer by dissolving 3 tablespoons of baking soda in 3 quarts (2.8L) of hot water and soak each cookware for 20 minutes. Then rinse with cold water and dry.
    <br>
    <br>
<strong>Spots and Stains: </strong>
<br>
1. Never use oven cleaners to clean cookware. They will ruin the cookware.
    <br>
<strong>2. Hard-Anodized</strong>: To lessen a stain, make a paste of baking soda and water, apply to cookware and scrub with a nonabrasive plastic mesh pad such as Scotch-Brite®. Do not use steel wool, coarse scouring pads or powder. Please note that plain hard anodized exterior cookware is porous. Staining will occur if food is spilled or allowed to boil over onto the exterior of the cookware.
    <br>
<strong>3. Stainless Steel or Nonstick</strong>: A spotted white film may form which can be removed with a mild solution of water and lemon juice or vinegar.
    <br>
<strong>4. Dishwasher</strong>: Some cookware materials are NOT dishwasher safe. Placing these types of cookware in the dishwasher will result in discoloration of your pans due to high water temperature and harsh detergents. This is not covered under warranty
    <br>
<strong>1. The following types of cookware materials are NOT dishwasher safe, Using them in the dishwasher will void your entire warranty</strong>: Hard-Anodized Aluminum cookware, Porcelain Enamel-exterior cookware, Copper Bottom cookware, Plain Aluminum cookware
    <br>
<strong>2. The following types of cookware materials are dishwasher safe</strong>: Stainless Steel cookware (with or without nonstick coating): Over time, the harsh dishwasher detergents may dull the stainless steel exterior. This is not covered under warranty.
    <br>
3. Aluminum cookware with exterior color coating (silicone polyester) with or without nonstick. However, the abrasive dishwasher detergent may discolour the cookware. Discoloring of the cookware caused by washing in a dishwasher is not a manufacturing defect and is not covered by the warranty.
<strong>4. Anodized Clad</strong>: Anodized Clad cookware is suitable for use in the dishwasher. However, as with all fine cookware, hand washing is recommended. In time, the cumulative effects of strong agitation and harsh detergent will affect the appearance of your pans, although the performance will not be affected and is not covered under warranty. Always follow dishwasher manufacturer’s instructions carefully.
    <br>
<strong>Storage</strong>: To avoid scratches or chips on the cookware exterior, place paper towels between cookware when storing.
    <br>
    <br>
<strong><a>HANDLES AND KNOBS</a></strong>
    <br>
    <br>
<span style="color: blue">Caution:</span>
    <br>
<strong>Hot handles and knobs</strong>: Handles and knobs can get very hot under some conditions. Use caution when touching them and always have potholders available for use.
<br>
<strong>Handle position when cooking</strong>: Position cookware so that handles are not over other hot burners. Do not allow handles to extend beyond the edge of the stove where the cookware can be knocked off the cooktop.
    <br>
<strong>Loose handles</strong>: Periodically, check handles and knobs to be sure they are not loose. If the handles are attached with screws, re-tighten the screws, being careful not to over-tighten. If the screw cannot be tightened, please contact Consumer Relations to order a new handle/knob. Handles that are attached with screws that are loose can separate from the pan and cause personal injury or property damage. Never use cookware that has loose handle.
    <br>
    <br>
    <strong><a>LIDS</a></strong>
<br>
<br>
<strong>Steam</strong>: When removing lids or cooking with steam vented lids, always position the lid so that the steam is directed away from you. Always use a potholder when adjusting lids with steam vents. Rising steam can cause burns.
    <br>
<strong>Locking lids</strong>: Make certain that the lid is locked securely when using teakettles, straining pots or other cookware with locking lids. This will prevent escaping steam or hot liquid, which will cause burns. Always double check that the lid is locked before straining or draining and always strain and drain away from your body. Do not use straining lids on other cookware pieces.
    <br>
<strong>Glass Lids</strong>:
<br>
<strong>1. Cracks and Scratches: Do not use glass lids that have cracks or scratches. If your lid is cracked or has deep scratches,breakage can occur spontaneously. Please contact Consumer Relations to order a new lid.</strong>
    <br>
<strong>2. Cleaning: Never use metal utensils, sharp instruments or harsh abrasives that may scratch and weaken the glass.
Temperature extremes: Do not place glass lids directly on top of, or directly under heating elements. Avoid extreme temperature changes when using glass lids. Do not submerge a hot lid in cold water.</strong>
    <br>
<strong>Lid vacuum</strong>: A lid left on cookware after turning down the heat or turning the burner off, may result in a vacuum that causes the lid to seal to the cookware. Using a lid made for another pan can also cause a lid vacuum. If a lid vacuum occurs, DO NOT attempt to remove the lid from cookware in any way. To avoid a lid seal, remove the lid or set it ajar before turning heat off.
    <br>
    <br>
    <strong><a>SPECIAL INSTRUCTIONS FOR CERAMIC/GLASS STOVETOPS</a></strong>
    <br>
    <br>
Always follow your stovetop manufacturer’s instructions for correct stovetop use and for specific cookware restrictions. We recommend the use of flat-bottomed stainless steel or hard-anodized exterior cookware on ceramic/glass cooktops. Overheating or allowing porcelain enameled or silicone polyester cookware to boil dry can result in the fusing of the cookware to your ceramic/glass stovetop requiring replacement of the stovetop. Meyer Canada will not be responsible for damage to stovetops.
Never place hot cookware on a cool ceramic/glass stovetop burner. This can also cause fusion of the pan to the stovetop. Before using double burner griddle or other large cookware pieces that are manufactured to sit over two stovetop burners, consult the stove manufacturer’s manual to ensure that your ceramic/glass stovetop has a bridge between the two burners you intend to use. If your stovetop does not have a bridge between the burners, DO NOT USE YOUR DOUBLE BURNER GRIDDLE OR OTHER LARGE COOKWARE PIECES- it may fuse to your stovetop, causing stovetop damage.
Cookware bases should be flat for even heat conduction. Decorative cookware bottoms may not conduct heat evenly. Do not drag or scrape cookware across your ceramic/glass stovetop. This can cause scratches or marks on your stovetop. Meyer Canada will not be responsible for stovetop damage.</a>
</div>
