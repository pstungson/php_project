<?php
    include_once './FeedbackController.php';
?>
<div class="container">
    <section class="bgwhite p-t-60 p-b-25">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9 p-b-80">
                    <div class="p-r-50 p-r-0-lg">
                        <!-- Leave a comment -->
                        <form class="leave-comment" method="post" action="FeedbackController.php">
                            <h4 class="m-text25 p-b-14">
                                Leave a Comment
                            </h4>

                            <p class="s-text8 p-b-40">
                                Your email address will not be published. Required fields are marked *
                            </p>

                            <textarea class="dis-block s-text7 size18 bo12 p-l-18 p-r-18 p-t-13 m-b-20" name="comment" placeholder="Comment..."></textarea>

                            <div class="bo12 of-hidden size19 m-b-20">
                                <input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="name" placeholder="Name *">
                            </div>

                            <div class="bo12 of-hidden size19 m-b-20">
                                <input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="email" placeholder="Email *">
                            </div>

                            <div class="w-size24">
                                <!-- Button -->
                                <button type="submit" name="submit" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    Post Comment
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>
<div class="container">
    <section class="bgwhite p-t-60 p-b-25">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9 p-b-80">
                    <div class="p-r-50 p-r-0-lg">
                        <!-- Leave a comment -->
                        <form class="leave-comment" method="post" action="FeedbackController.php">
                            <h4 class="m-text25 p-b-14">
                                Comment
                            </h4>
                            <p class="s-text8 p-b-40">
                                Your contributions, comments will help improve our website.
                            </p>
                            <ul>
                                <?php
                                    $feedback = getTopFeedBack();
                                    foreach ($feedback as $value) {
                                        $mafeedback = $value['id'];
                                        $name = $value['name'];
                                        $email = $value['email'];
                                        $content = $value['content'];
                                    ?>
                                    <li>
                                        <b><?php echo $name;?></b>
                                        <ul>
                                            <li style="margin-left:20px"> <?php echo $content;?> </li>
                                        </ul>
                                    </li>
                                    <?php }?>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $feedback = postFeedback();?>
