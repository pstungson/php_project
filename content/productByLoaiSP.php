<?php
    include_once './SanPhamController.php';
    include_once './CategoryController.php';
    $id = $_GET['id'];
    $category = getDetailCategory($id);
?>
<div class="container">
    <section class="blog bgwhite p-t-94 p-b-65">
        <div class="container">
            <div class="sec-title p-b-52">
                <h3 class="m-text5 t-center">
                    Products of <?php echo $category['name'] ?>
                </h3>
            </div>
            <div class="row">
                <?php
                    $sanPhamByLoai = getSanPhamByLoai();
                    foreach ($sanPhamByLoai as $value) {
                        $masp = $value['id'];
                        $tensp = $value['name_pr'];
                        $mota = $value['detail'];
                        $anh = $value['link_img'];
                ?>
                <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
                    <!-- Block3 -->
                    <div class="block3">
                        <a href="index.php?page=product&id=<?php echo $masp;?>" class="block3-img dis-block hov-img-zoom">
                            <img src="img/<?php echo $anh?>" style="height:220px" alt="IMG-BLOG">
                        </a>
                        <div class="block3-txt p-t-14">
                            <h4 class="p-b-7">
                                <a href="index.php?page=product&id=<?php echo $masp;?>" class="m-text11">
                                    <?php echo $tensp;  ?>
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>
    </section>
</div>
